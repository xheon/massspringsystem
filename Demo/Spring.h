#ifndef MASSSPRINGSYSTEM_SPRING_H
#define MASSSPRINGSYSTEM_SPRING_H

namespace MassSpringSystem
{
	class Spring
	{
	public:
		Spring(float stiffness, Point* point1, Point* point2);
		~Spring();

		void EvaluateForces();
		void ClearForces();

		void SetPoint1( Point* point );
		void SetPoint2( Point* point );
		void SetStiffness( float stiffness );

		Point* GetPoint1();
		Point* GetPoint2();
		float GetStiffness();
		float GetInitialLength();
		float GetCurrentLength();

	private:
		Point* pPoint1_;
		Point* pPoint2_;
		float fStiffness_;
		float fInitialLength_;
		float fCurrentLength_;
	};
}

#endif