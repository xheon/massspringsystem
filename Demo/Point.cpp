#include <DXUT.h>
#include "Point.h"

namespace MassSpringSystem
{
	Point::Point(XMFLOAT3 pos)
	{
		position_ = pos;
	}

	Point::~Point()
	{
		//delete pSphere_;
	}

	void Point::ComputeTotalForce()
	{
		XMVECTOR vInternalForce = XMLoadFloat3( &internalForce_ );
		XMVECTOR vExternalForce = XMLoadFloat3( &externalForce_ );
		XMVECTOR vVelocity = XMLoadFloat3( &velocity_ );
		XMVECTOR vTotalForce;

		vTotalForce = vExternalForce + vInternalForce - vVelocity * fDamping_;

		XMStoreFloat3( &totalForce_, vTotalForce );
	}

	void Point::UpdateVelocity( float fDeltaTime )
	{
		if (bIsFixed_ == false)
		{
			XMVECTOR vOldVelocity = XMLoadFloat3(&velocity_);
			XMVECTOR vTotalForce = XMLoadFloat3(&totalForce_);

			XMVECTOR vVelocity = vOldVelocity + fDeltaTime * vTotalForce / fMass_;

			XMStoreFloat3(&velocity_, vVelocity);
			//XMStoreFloat3( &oldVelocity_, vOldVelocity );
		}
	}

	void Point::UpdatePosition(float fDeltaTime)
	{
		if ( bIsFixed_ == false )
		{
			XMVECTOR pos = XMLoadFloat3( &position_ );
			XMVECTOR currentPosition = pos + fDeltaTime * XMLoadFloat3( &velocity_ );
			XMStoreFloat3( &position_, currentPosition );

			// Boundary condition for the ground
			if ( position_.y <= -0.5f )
			{
				velocity_.y = fabs(velocity_.y);
				pos += fDeltaTime * XMLoadFloat3( &velocity_ );

				XMStoreFloat3( &position_, pos );
			}
		}
	}

	void Point::MidPointStepTemp( float fDeltaTime )
	{
		if (bIsFixed_ == false)
		{
			XMVECTOR position = XMLoadFloat3(&position_);
			XMVECTOR velocity = XMLoadFloat3(&velocity_);
			XMVECTOR vTotalForce = XMLoadFloat3( &totalForce_ );

			XMVECTOR positionTemp = position + (fDeltaTime / 2) * velocity;
			XMVECTOR velocityTemp = velocity + (fDeltaTime / 2) * (vTotalForce / fMass_);

			XMStoreFloat3(&oldPosition_, position);
			XMStoreFloat3(&oldVelocity_, velocity);

			XMStoreFloat3(&position_, positionTemp);
			XMStoreFloat3(&velocity_, velocityTemp);
		}
	}

	void Point::MidPointStep(float fDeltaTime) 
	{
		if (bIsFixed_ == false) {
			XMVECTOR oldPosition = XMLoadFloat3(&oldPosition_);
			XMVECTOR oldVelocity = XMLoadFloat3(&oldVelocity_);
			XMVECTOR vTotalForce = XMLoadFloat3( &totalForce_ );


			XMVECTOR position = oldPosition + fDeltaTime * XMLoadFloat3( &velocity_ );
			XMVECTOR velocity = oldVelocity + fDeltaTime * (vTotalForce / fMass_);

			XMStoreFloat3(&position_, position);
			XMStoreFloat3(&velocity_, velocity);

			// Boundary condition for the ground
			if ( position_.y <= -0.5f )
			{
				velocity_.y = fabs( velocity_.y);
				oldPosition += fDeltaTime * XMLoadFloat3(&velocity_);

				XMStoreFloat3( &position_, oldPosition );

				//printf( "Touch ground: %f\n", velocity_.y );
			}
		}

		
	}

	void Point::SetSphere (GeometricPrimitive* pSphere)
	{
		pSphere_ = pSphere;
	}

	void Point::SetPosition (XMFLOAT3 position)
	{
		position_ = position;
	}

	void Point::SetVelocity (XMFLOAT3 velocity)
	{
		velocity_ = velocity;
	}

	void Point::SetInternalForce (XMFLOAT3 force)
	{
		internalForce_ = force;
	}

	void Point::SetExternalForce( XMFLOAT3 force )
	{
		externalForce_ = force;
	}

	void Point::SetMass (float mass)
	{
		fMass_ = mass;
	}

	void Point::SetIsFixed (bool isFixed)
	{
		bIsFixed_ = isFixed;
	}

	void Point::SetDamping (float damping)
	{
		fDamping_ = damping;
	}

	GeometricPrimitive* Point::GetSphere ()
	{
		return pSphere_;
	}

	XMFLOAT3 Point::GetPosition ()
	{
		return position_;
	}

	XMFLOAT3 Point::GetVelocity ()
	{
		return velocity_;
	}

	XMFLOAT3 Point::GetInternalForce ()
	{
		return internalForce_;
	}

	XMFLOAT3 Point::GetExternalForce()
	{
		return externalForce_;
	}

	XMFLOAT3 Point::GetForce()
	{
		XMVECTOR force = XMLoadFloat3( &internalForce_ ) + XMLoadFloat3( &externalForce_ );
		XMFLOAT3 totalForce;
		XMStoreFloat3( &totalForce, force );
		return totalForce;
	}

	float Point::GetMass ()
	{
		return fMass_;
	}

	float Point::GetDamping ()
	{
		return fDamping_;
	}

	bool Point::IsFixed ()
	{
		return bIsFixed_;
	}
}
