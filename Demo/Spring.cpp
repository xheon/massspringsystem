#include "Point.h"
#include "Spring.h"

namespace MassSpringSystem
{
	Spring::Spring(float stiffness, Point* point1, Point* point2)
	{
		fStiffness_ = stiffness;
		pPoint1_ = point1;
		pPoint2_ = point2;
		
		XMVECTOR pos1 = XMLoadFloat3 (&pPoint1_->GetPosition());
		XMVECTOR pos2 = XMLoadFloat3 (&pPoint2_->GetPosition());

		fInitialLength_ = XMVectorGetX(XMVector3Length(pos1-pos2));
		fCurrentLength_ = fInitialLength_;
	}

	Spring::~Spring()
	{}

	void Spring::EvaluateForces()
	{
		XMVECTOR currentForce1 = XMLoadFloat3( &pPoint1_->GetInternalForce() );
		XMVECTOR currentForce2 = XMLoadFloat3( &pPoint2_->GetInternalForce() );
		XMVECTOR point1Position = XMLoadFloat3 (&pPoint1_->GetPosition ());
		XMVECTOR point2Position = XMLoadFloat3 (&pPoint2_->GetPosition ());
		
		// Update current lenght
		fCurrentLength_ = XMVectorGetX(XMVector3Length(point1Position-point2Position));
		
		XMVECTOR newForce1 = - fStiffness_ * (fCurrentLength_ - fInitialLength_) 
			* ((point1Position - point2Position) / fCurrentLength_);

		XMFLOAT3 vNewForce1;
		XMStoreFloat3 (&vNewForce1, currentForce1 + newForce1);
		pPoint1_->SetInternalForce (vNewForce1);

		XMVECTOR newForce2 = -newForce1;

		XMFLOAT3 vNewForce2;
		XMStoreFloat3 (&vNewForce2, currentForce2 + newForce2);
		pPoint2_->SetInternalForce (vNewForce2);

		//printf("Spring: %f, %f\n", fCurrentLength_, vNewForce2.y);
		//printf( "F= - %f * (%f - %f) = %f\n", fStiffness_, fCurrentLength_, fInitialLength_, vNewForce1.y );
		
		//printf("X: %f, Y: %f, Z: %f\n", vNewForce2.x, vNewForce2.y, vNewForce2.z);
		//printf("X: %f, Y: %f, Z: %f\n", vNewForce1.x, vNewForce1.y, vNewForce1.z);

		/*fCurrentLength_ = pPoint1_->GetPosition().y - pPoint2_->GetPosition().y;

		float fDeltaLength = fCurrentLength_ - fInitialLength_;
		float forceY = - fStiffness_ * (fCurrentLength_ - fInitialLength_);
		pPoint1_->SetInternalForce(XMFLOAT3(0, forceY, 0));
		pPoint2_->SetInternalForce(XMFLOAT3(0, -forceY, 0));
		printf("F = - %f * (%f - %f) = %f\n", fStiffness_, fCurrentLength_, fInitialLength_, forceY);*/
		//printf("Internal Force: %f", forceY);
		//printf("External Force : %f\n", pPoint2_->GetExternalForce().y);
	}

	void Spring::ClearForces()
	{
		pPoint1_->SetInternalForce(XMFLOAT3(0, 0, 0));
		pPoint2_->SetInternalForce(XMFLOAT3(0, 0, 0));
		pPoint1_->SetExternalForce(XMFLOAT3(0, 0, 0));
		pPoint2_->SetExternalForce(XMFLOAT3(0, 0, 0));
	}

	void Spring::SetPoint1(Point* point)
	{
		pPoint1_ = point;
	}

	void Spring::SetPoint2(Point* point)
	{
		pPoint2_ = point;
	}

	void Spring::SetStiffness(float stiffness)
	{
		fStiffness_ = stiffness;
	}

	Point* Spring::GetPoint1()
	{
		return pPoint1_;
	}

	Point* Spring::GetPoint2()
	{
		return pPoint2_;
	}

	float Spring::GetStiffness()
	{
		return fStiffness_;
	}

	float Spring::GetInitialLength()
	{
		return fInitialLength_;
	}

	float Spring::GetCurrentLength()
	{
		return fCurrentLength_;
	}
}
