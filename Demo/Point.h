#ifndef MASSSPRINGSYSTEM_POINT_H
#define MASSSPRINGSYSTEM_POINT_H

#include <DirectXMath.h>
#include "GeometricPrimitive.h"

using namespace DirectX;

namespace MassSpringSystem
{
	class Point
	{
	public:
		Point( XMFLOAT3 pos );
		~Point();

		void UpdateVelocity(float fTime);
		void UpdatePosition(float fDeltaTime);

		void MidPointStepTemp(float fDeltaTime);
		void MidPointStep( float fDeltaTime );

		void SetSphere( GeometricPrimitive* pSphere );
		void SetPosition( XMFLOAT3 position );
		void SetVelocity( XMFLOAT3 velocity );
		void SetInternalForce( XMFLOAT3 force );
		void SetExternalForce( XMFLOAT3 force );
		void SetMass( float mass );
		void SetIsFixed( bool isFixed );
		void SetDamping( float damping );

		GeometricPrimitive* GetSphere();
		XMFLOAT3 GetPosition();
		XMFLOAT3 GetVelocity();
		XMFLOAT3 GetInternalForce();
		XMFLOAT3 GetExternalForce();
		XMFLOAT3 GetForce();
		float GetMass();
		float GetDamping();
		bool IsFixed();

		void ComputeTotalForce();

		GeometricPrimitive* pSphere_;

	private:
		
		XMFLOAT3 position_ = XMFLOAT3(0, 0, 0);
		XMFLOAT3 velocity_ = XMFLOAT3(0, 0, 0);
		XMFLOAT3 externalForce_ = XMFLOAT3(0,0,0);
		XMFLOAT3 internalForce_ = XMFLOAT3(0,0,0);
		XMFLOAT3 totalForce_ = XMFLOAT3( 0, 0, 0 );

		XMFLOAT3 oldPosition_ = XMFLOAT3(0, 0, 0);
		XMFLOAT3 oldVelocity_ = XMFLOAT3(0, 0, 0);

		float fMass_ = 1.0f;
		float fDamping_ = 0.0f;
		bool bIsFixed_ = false;
	};
}

#endif