//--------------------------------------------------------------------------------------
// File: main.cpp
//
// The main file containing the entry point main().
//--------------------------------------------------------------------------------------

#include <sstream>
#include <iomanip>
#include <random>
#include <iostream>

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"

// Own includes
#include "Point.h"
#include "Spring.h"

// DXUT camera
// NOTE: CModelViewerCamera does not only manage the standard view transformation/camera position 
//       (CModelViewerCamera::GetViewMatrix()), but also allows for model rotation
//       (CModelViewerCamera::GetWorldMatrix()). 
//       Look out for CModelViewerCamera::SetButtonMasks(...).
CModelViewerCamera g_camera;

// Effect corresponding to "effect.fx"
ID3DX11Effect* g_pEffect = nullptr;

// Main tweak bar
TwBar* g_pTweakBar;

// DirectXTK effects, input layouts and primitive batches for different vertex types
BasicEffect*                               g_pEffectPositionColor               = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionColor          = nullptr;
PrimitiveBatch<VertexPositionColor>*       g_pPrimitiveBatchPositionColor       = nullptr;
                                                                                
BasicEffect*                               g_pEffectPositionNormal              = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormal         = nullptr;
PrimitiveBatch<VertexPositionNormal>*      g_pPrimitiveBatchPositionNormal      = nullptr;
                                                                                
BasicEffect*                               g_pEffectPositionNormalColor         = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormalColor    = nullptr;
PrimitiveBatch<VertexPositionNormalColor>* g_pPrimitiveBatchPositionNormalColor = nullptr;

// DirectXTK simple geometric primitives
std::unique_ptr<GeometricPrimitive> g_pSphere;
std::unique_ptr<GeometricPrimitive> g_pTeapot;

std::vector<MassSpringSystem::Point> g_Points;
std::vector<MassSpringSystem::Spring> g_Springs;

// Movable object management
XMINT2   g_viMouseDelta = XMINT2(0,0);
XMFLOAT3 g_vfMovableObjectPos = XMFLOAT3(0,0,0);

// TweakAntBar GUI variables
int   g_iNumSpheres    = 3;
float g_fSphereSize = 0.10f;
float g_fGravity = -9.81f;
float g_fStiffness = 40.0f;
bool  g_bDrawTeapot    = false;
bool  g_bDrawTriangle  = false;
bool  g_bDrawSpheres   = true;
float g_fDeltaTime = 1/64.0f;
float g_fTimeAcc = 0.0f;
float g_Time = 0.0f;
bool g_bNextStep = false;
bool g_bEuler = 0;
float g_fDamping = 0.7f;
int g_iActivePoint = 0;
int g_iOldActivePoint = 0;
float g_fActivePointMass = 1.0f;
bool g_bSetup = false;
bool g_bSetupOld = false;

// Video recorder
FFmpeg* g_pFFmpegVideoRecorder = nullptr;

// Create TweakBar and add required buttons and variables
void InitTweakBar( ID3D11Device* pd3dDevice )
{
	g_fActivePointMass = g_Points[g_iActivePoint].GetMass();
	
	std::string maxAcitvePoint = "min=0 max=";
	maxAcitvePoint.append( std::to_string( g_Points.size() -1));
	
	TwInit(TW_DIRECT3D11, pd3dDevice);

    g_pTweakBar = TwNewBar("TweakBar");

    // HINT: For buttons you can directly pass the callback function as a lambda expression.
    TwAddButton(g_pTweakBar, "Reset Camera", [](void *){g_camera.Reset();}, nullptr, "");
	TwAddVarRW( g_pTweakBar, "Integration", TW_TYPE_BOOLCPP, &g_bEuler, "true='Euler' false='Midpoint'" );
	TwAddVarRW( g_pTweakBar, "Change Setup", TW_TYPE_BOOLCPP, &g_bSetup, "true='1 ' false='2 '" );
    TwAddVarRW(g_pTweakBar, "Draw Spheres",  TW_TYPE_BOOLCPP, &g_bDrawSpheres, "");
    TwAddVarRW(g_pTweakBar, "Gravity", TW_TYPE_FLOAT, &g_fGravity, "step=0.01");
	TwAddVarRW(g_pTweakBar, "Stiffness", TW_TYPE_FLOAT, &g_fStiffness, "min=0.01 step=0.01");
	TwAddVarRW(g_pTweakBar, "Damping", TW_TYPE_FLOAT, &g_fDamping, "min=0.01 step=0.01");
	TwAddVarRW(g_pTweakBar, "Active Point", TW_TYPE_INT32, &g_iActivePoint, maxAcitvePoint.c_str());
	TwAddVarRW(g_pTweakBar, "Point Mass", TW_TYPE_FLOAT, &g_fActivePointMass, "min=0.1 step=0.1");
	
}

void EulerStep()
{
	for (int i = 0; i < g_Springs.size(); i++)
	{
		g_Springs[i].ClearForces();
	}

	for (int i = 0; i < g_Springs.size(); i++)
	{
		g_Springs[i].SetStiffness(g_fStiffness);
		g_Springs[i].EvaluateForces();
	}

	for (int i = 0; i < g_Points.size(); i++)
	{
		g_Points[i].SetDamping(g_fDamping);
		if (i == g_iActivePoint)
		{
			g_Points[i].SetExternalForce(XMFLOAT3(g_vfMovableObjectPos.x, g_fGravity + g_vfMovableObjectPos.y, g_vfMovableObjectPos.z));
		}
		else
		{
			g_Points[i].SetExternalForce(XMFLOAT3(0, g_fGravity, 0));
		}
		g_Points[i].ComputeTotalForce();
	}
	
	for ( int i = 0; i < g_Points.size(); i++ )
	{
		g_Points[i].UpdatePosition( g_fDeltaTime );
	}

	for ( int i = 0; i < g_Points.size(); i++ )
	{
		g_Points[i].UpdateVelocity( g_fDeltaTime );
	}
}

void MidpointStep()
{
	for ( int i = 0; i < g_Points.size(); i++ )
	{
		g_Points[i].MidPointStepTemp( g_fDeltaTime );
	}

	for ( int i = 0; i < g_Springs.size(); i++ )
	{
		g_Springs[i].ClearForces();
	}

	for ( int i = 0; i < g_Springs.size(); i++ )
	{
		g_Springs[i].SetStiffness( g_fStiffness );
		g_Springs[i].EvaluateForces();
	}

	for ( int i = 0; i < g_Points.size(); i++ )
	{
		g_Points[i].SetDamping( g_fDamping );

		if (i == g_iActivePoint)
		{
			g_Points[i].SetExternalForce(XMFLOAT3(g_vfMovableObjectPos.x, g_fGravity + g_vfMovableObjectPos.y, g_vfMovableObjectPos.z));
		}
		else
		{
			g_Points[i].SetExternalForce(XMFLOAT3(0, g_fGravity,0));
		}
		g_Points[i].ComputeTotalForce();
		g_Points[i].MidPointStep( g_fDeltaTime );
	}

	for ( int i = 0; i < g_Springs.size(); i++ )
	{
		g_Springs[i].ClearForces();
	}

	for ( int i = 0; i < g_Springs.size(); i++ )
	{
		g_Springs[i].EvaluateForces();
	}

	for ( int i = 0; i < g_Points.size(); i++ )
	{
		g_Points[i].SetDamping( g_fDamping );
		if (i == g_iActivePoint)
		{
			g_Points[i].SetExternalForce(XMFLOAT3(g_vfMovableObjectPos.x, g_fGravity + g_vfMovableObjectPos.y, g_vfMovableObjectPos.z));
		}
		else
		{
			g_Points[i].SetExternalForce(XMFLOAT3(0, g_fGravity, 0));
		}
		g_Points[i].ComputeTotalForce();
	}
}

// Draw the edges of the bounding box [-0.5;0.5]� rotated with the cameras model tranformation.
// (Drawn as line primitives using a DirectXTK primitive batch)
void DrawBoundingBox(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/color effect
    g_pEffectPositionColor->SetWorld(g_camera.GetWorldMatrix());
    
    g_pEffectPositionColor->Apply(pd3dImmediateContext);
    pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

    // Draw
    g_pPrimitiveBatchPositionColor->Begin();
    
    // Lines in x direction (red color)
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet(-0.5f, (float)(i%2)-0.5f, (float)(i/2)-0.5f, 1), Colors::Red),
            VertexPositionColor(XMVectorSet( 0.5f, (float)(i%2)-0.5f, (float)(i/2)-0.5f, 1), Colors::Red)
        );
    }

    // Lines in y direction
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, -0.5f, (float)(i/2)-0.5f, 1), Colors::Green),
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f,  0.5f, (float)(i/2)-0.5f, 1), Colors::Green)
        );
    }

    // Lines in z direction
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, (float)(i/2)-0.5f, -0.5f, 1), Colors::Blue),
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, (float)(i/2)-0.5f,  0.5f, 1), Colors::Blue)
        );
    }

    g_pPrimitiveBatchPositionColor->End();
}

// Draw a large, square plane at y=-1 with a checkerboard pattern
// (Drawn as multiple quads, i.e. triangle strips, using a DirectXTK primitive batch)
void DrawFloor(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal/color effect
    g_pEffectPositionNormalColor->SetWorld(XMMatrixIdentity());
    g_pEffectPositionNormalColor->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormalColor->SetDiffuseColor(0.8f * Colors::White);
    g_pEffectPositionNormalColor->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormalColor->SetSpecularPower(1000);

    g_pEffectPositionNormalColor->Apply(pd3dImmediateContext);
    pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionNormalColor);

    // Draw 4*n*n quads spanning x = [-n;n], y = -1, z = [-n;n]
    const float n = 4;
    XMVECTOR normal      = XMVectorSet(0, 1,0,0);
    XMVECTOR planecenter = XMVectorSet(0, -0.5f,0,0);

    g_pPrimitiveBatchPositionNormalColor->Begin();
    for (float z = -n; z < n; z++)
    {
        for (float x = -n; x < n; x++)
        {
            // Quad vertex positions
            XMVECTOR pos[] = { XMVectorSet(x  , -0.5f, z+1, 0),
                               XMVectorSet(x+1, -0.5f, z+1, 0),
                               XMVectorSet(x+1, -0.5f, z  , 0),
                               XMVectorSet(x  , -0.5f, z  , 0) };

            // Color checkerboard pattern (white & gray)
            XMVECTOR color = ((int(z + x) % 2) == 0) ? XMVectorSet(1,1,1,1) : XMVectorSet(0.6f,0.6f,0.6f,1);

            // Color attenuation based on distance to plane center
            float attenuation[] = {
                1.0f - XMVectorGetX(XMVector3Length(pos[0] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[1] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[2] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[3] - planecenter)) / n };

            g_pPrimitiveBatchPositionNormalColor->DrawQuad(
                VertexPositionNormalColor(pos[0], normal, attenuation[0] * color),
                VertexPositionNormalColor(pos[1], normal, attenuation[1] * color),
                VertexPositionNormalColor(pos[2], normal, attenuation[2] * color),
                VertexPositionNormalColor(pos[3], normal, attenuation[3] * color)
            );
        }
    }
    g_pPrimitiveBatchPositionNormalColor->End();    
}

// Draw several objects randomly positioned in [-0.5f;0.5]�  using DirectXTK geometric primitives.
void DrawPoints(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal effect (constant variables)
    g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormal->SetSpecularPower(100);
      
    std::mt19937 eng;
    std::uniform_real_distribution<float> randCol( 0.0f, 1.0f);
    std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);

    //for (int i=0; i<g_iNumSpheres; i++)
    for (int i=0; i<g_Points.size(); i++)
    {
        
        // Setup position/normal effect (per object variables)
		if (i == g_iActivePoint)
		{
			g_pEffectPositionNormal->SetDiffuseColor(0.6f * XMColorHSVToRGB(XMVectorSet(1, 1, 1, 0)));

		}
		else
		{
			g_pEffectPositionNormal->SetDiffuseColor(0.6f * XMVectorSet(1, 1, 0, 0));
		}
		XMMATRIX scale = XMMatrixScaling(2*g_fSphereSize * g_Points[i].GetMass(),2 * g_fSphereSize * g_Points[i].GetMass(), 2*g_fSphereSize *  g_Points[i].GetMass());
        XMMATRIX trans = XMMatrixTranslation(g_Points[i].GetPosition ().x,
			g_Points[i].GetPosition ().y,
			g_Points[i].GetPosition ().z);
		g_pEffectPositionNormal->SetWorld(scale * trans);// *g_camera.GetWorldMatrix());

        // Draw
        // NOTE: The following generates one draw call per object, so performance will be bad for n>>1000 or so
        //g_Points[]
        g_Points[i].GetSphere ()->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
            //.g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
    }
}


void DrawSpring( ID3D11DeviceContext* pd3dImmediateContext )
{
	// Setup position/color effect
	g_pEffectPositionColor->SetWorld( g_camera.GetWorldMatrix() );

	g_pEffectPositionColor->Apply( pd3dImmediateContext );
	pd3dImmediateContext->IASetInputLayout( g_pInputLayoutPositionColor );

	g_pPrimitiveBatchPositionColor->Begin();

	for ( int i = 0; i < g_Springs.size(); i++ )
	{
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor( XMVectorSet(
			g_Springs[i].GetPoint1()->GetPosition().x,
			g_Springs[i].GetPoint1()->GetPosition().y,
			g_Springs[i].GetPoint1()->GetPosition().z, 1 ), Colors::Red ),
			VertexPositionColor( XMVectorSet(
			g_Springs[i].GetPoint2()->GetPosition().x,
			g_Springs[i].GetPoint2()->GetPosition().y,
			g_Springs[i].GetPoint2()->GetPosition().z, 1 ), Colors::Red ) );
	}

	g_pPrimitiveBatchPositionColor->End();
}

void InitMassSpringSystem(ID3D11DeviceContext* pd3dImmediateContext)
{
	for ( int i = 0; i < g_Points.size(); i++ )
	{
		delete g_Points[i].GetSphere() ;
	}
	
	g_Points.clear();
	g_Springs.clear();

	if ( g_bSetup == true )
	{
		g_Points.emplace_back( XMFLOAT3( 0.5f, 2.5f, 0.0f ) );
		g_Points.emplace_back( XMFLOAT3( 0.0f, 1.5f, 0.0f ) );
		g_Points.emplace_back( XMFLOAT3( -0.5f, 2.5f, 0.0f ) );
		g_Points.emplace_back( XMFLOAT3( 0.0f, 0.5f, 0. - 0.5f ) );

		// Setup for collision
		g_Points.emplace_back( XMFLOAT3( 2.0f, 2.0f, 0.0f ) );
		g_Points.emplace_back( XMFLOAT3( 1.5f, 1.0f, 0.0f ) );
		g_Points.emplace_back( XMFLOAT3( 2.5f, 1.0f, 0.0f ) );

		g_Points[0].SetIsFixed( true );
		g_Points[2].SetIsFixed( true );
		g_Points[4].SetIsFixed( true );
		for ( int i = 0; i < g_Points.size(); i++ )
		{
			g_Points[i].SetExternalForce( XMFLOAT3( 0, g_fGravity, 0 ) );

			if ( g_Points[i].IsFixed() == true )
			{
				g_Points[i].SetSphere( GeometricPrimitive::CreateCube( pd3dImmediateContext, 2.0f, false ).release() );
			}
			else
			{
				g_Points[i].SetSphere( GeometricPrimitive::CreateGeoSphere( pd3dImmediateContext, g_Points[i].GetMass(), 2, false ).release() );
			}
		}




		g_Springs.emplace_back( g_fStiffness, &g_Points[0], &g_Points[1] );
		g_Springs.emplace_back( g_fStiffness, &g_Points[2], &g_Points[1] );
		g_Springs.emplace_back( g_fStiffness, &g_Points[1], &g_Points[3] );

		g_Springs.emplace_back( g_fStiffness, &g_Points[4], &g_Points[5] );
		g_Springs.emplace_back( g_fStiffness, &g_Points[4], &g_Points[6] );

	}
	// setup 2 (Gitter)
	else
	{

		//g_Points.emplace_back( XMFLOAT3( 0, 1, 0 ) );
		//g_Points[0].SetMass( 4 );
		//g_Points[0].SetIsFixed (true);

		int width = 8;
		int height = 9;

		for ( int i = 0; i < 8; ++i )
		{
			g_Points.emplace_back( XMFLOAT3( i, 12, 0 ) );
			g_Points[i].SetIsFixed( true );
		}

		for ( int i = 1; i < 9; ++i )
		{
			for ( int j = 0; j < width; ++j )
			{
				g_Points.emplace_back( XMFLOAT3( j, height - i+3, 0 ) );
			}
		}

		for ( int i = 0; i < g_Points.size(); i++ )
		{
			g_Points[i].SetExternalForce( XMFLOAT3( 0, g_fGravity, 0 ) );

			if ( g_Points[i].IsFixed() == true )
			{
				g_Points[i].SetSphere( GeometricPrimitive::CreateCube( pd3dImmediateContext, 2.0f, false ).release() );
			}
			else
			{
				g_Points[i].SetSphere( GeometricPrimitive::CreateGeoSphere( pd3dImmediateContext, g_Points[i].GetMass(), 2, false ).release() );
			}
		}

		for ( int i = 0; i < width; ++i )
		{
			for ( int j = 0; j < height; ++j )
			{
				int index = i + width * j;
				if ( (index + 1) < width * height && (i + 1) < width )
				{
					g_Springs.emplace_back( g_fStiffness, &g_Points[index], &g_Points[index + 1] );
				}

				if ( (index + width) < width * height )
				{
					g_Springs.emplace_back( g_fStiffness, &g_Points[index], &g_Points[index + width] );
				}

				if ( (index + width + 1) < width * height && (i + 1) < width )
				{
					g_Springs.emplace_back( g_fStiffness, &g_Points[index], &g_Points[index + width + 1] );
				}

				if ( (index + width - 1) < width * height && i > 0 )
				{
					g_Springs.emplace_back( g_fStiffness, &g_Points[index], &g_Points[index + width - 1] );
				}
			}
		}

	}

}




// ============================================================
// DXUT Callbacks
// ============================================================


//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
                                       DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
    return true;
}


//--------------------------------------------------------------------------------------
// Called right before creating a device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
    return true;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependent on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
    HRESULT hr;

    ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();;

    std::wcout << L"Device: " << DXUTGetDeviceStats() << std::endl;
    
    // Load custom effect from "effect.fxo" (compiled "effect.fx")
    std::wstring effectPath = GetExePath() + L"effect.fxo";
    if(FAILED(hr = D3DX11CreateEffectFromFile(effectPath.c_str(), 0, pd3dDevice, &g_pEffect)))
    {
        std::wcout << L"Failed creating effect with error code " << int(hr) << std::endl;
        return hr;
    }

	InitMassSpringSystem( pd3dImmediateContext );


    // Init AntTweakBar GUI
    InitTweakBar(pd3dDevice);

    // Create DirectXTK geometric primitives for later usage
    g_pSphere = GeometricPrimitive::CreateGeoSphere(pd3dImmediateContext, 2.0f, 2, false);
    g_pTeapot = GeometricPrimitive::CreateTeapot(pd3dImmediateContext, 1.5f, 8, false);

    // Create effect, input layout and primitive batch for position/color vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionColor = new BasicEffect(pd3dDevice);
        g_pEffectPositionColor->SetVertexColorEnabled(true); // triggers usage of position/color vertices

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);
        
        pd3dDevice->CreateInputLayout(VertexPositionColor::InputElements,
                                      VertexPositionColor::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionColor);

        // Primitive batch
        g_pPrimitiveBatchPositionColor = new PrimitiveBatch<VertexPositionColor>(pd3dImmediateContext);
    }

    // Create effect, input layout and primitive batch for position/normal vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionNormal = new BasicEffect(pd3dDevice);
        g_pEffectPositionNormal->EnableDefaultLighting(); // triggers usage of position/normal vertices
        g_pEffectPositionNormal->SetPerPixelLighting(true);

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionNormal->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

        pd3dDevice->CreateInputLayout(VertexPositionNormal::InputElements,
                                      VertexPositionNormal::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionNormal);

        // Primitive batch
        g_pPrimitiveBatchPositionNormal = new PrimitiveBatch<VertexPositionNormal>(pd3dImmediateContext);
    }

    // Create effect, input layout and primitive batch for position/normal/color vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionNormalColor = new BasicEffect(pd3dDevice);
        g_pEffectPositionNormalColor->SetPerPixelLighting(true);
        g_pEffectPositionNormalColor->EnableDefaultLighting();     // triggers usage of position/normal/color vertices
        g_pEffectPositionNormalColor->SetVertexColorEnabled(true); // triggers usage of position/normal/color vertices

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionNormalColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

        pd3dDevice->CreateInputLayout(VertexPositionNormalColor::InputElements,
                                      VertexPositionNormalColor::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionNormalColor);

        // Primitive batch
        g_pPrimitiveBatchPositionNormalColor = new PrimitiveBatch<VertexPositionNormalColor>(pd3dImmediateContext);
    }

    return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
    SAFE_RELEASE(g_pEffect);
    
    TwDeleteBar(g_pTweakBar);
    g_pTweakBar = nullptr;
    TwTerminate();

    for(int i = 0; i < g_Points.size(); i++)
    {
        SAFE_DELETE(g_Points[i].pSphere_);
    }

    g_pSphere.reset();
    g_pTeapot.reset();
    
    SAFE_DELETE (g_pPrimitiveBatchPositionColor);
    SAFE_RELEASE(g_pInputLayoutPositionColor);
    SAFE_DELETE (g_pEffectPositionColor);

    SAFE_DELETE (g_pPrimitiveBatchPositionNormal);
    SAFE_RELEASE(g_pInputLayoutPositionNormal);
    SAFE_DELETE (g_pEffectPositionNormal);

    SAFE_DELETE (g_pPrimitiveBatchPositionNormalColor);
    SAFE_RELEASE(g_pInputLayoutPositionNormalColor);
    SAFE_DELETE (g_pEffectPositionNormalColor);
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
                                          const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
    // Update camera parameters
    int width = pBackBufferSurfaceDesc->Width;
    int height = pBackBufferSurfaceDesc->Height;
    g_camera.SetWindow(width, height);
    g_camera.SetProjParams(XM_PI / 4.0f, float(width) / float(height), 0.1f, 100.0f);

    // Inform AntTweakBar about back buffer resolution change
    TwWindowSize(pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height);

    return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{
}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
    HRESULT hr;

    if(bKeyDown)
    {
        switch(nChar)
        {
            // RETURN: toggle fullscreen
            case VK_RETURN :
            {
                if(bAltDown) DXUTToggleFullScreen();
                break;
            }
            // F8: Take screenshot
            case VK_F8:
            {
                // Save current render target as png
                static int nr = 0;
                std::wstringstream ss;
                ss << L"Screenshot" << std::setfill(L'0') << std::setw(4) << nr++ << L".png";

                ID3D11Resource* pTex2D = nullptr;
                DXUTGetD3D11RenderTargetView()->GetResource(&pTex2D);
                SaveWICTextureToFile(DXUTGetD3D11DeviceContext(), pTex2D, GUID_ContainerFormatPng, ss.str().c_str());
                SAFE_RELEASE(pTex2D);

                std::wcout << L"Screenshot written to " << ss.str() << std::endl;
                break;
            }
            // F10: Toggle video recording
            case VK_F10:
            {
                if (!g_pFFmpegVideoRecorder) {
                    g_pFFmpegVideoRecorder = new FFmpeg(25, 21, FFmpeg::MODE_INTERPOLATE);
                    V(g_pFFmpegVideoRecorder->StartRecording(DXUTGetD3D11Device(), DXUTGetD3D11RenderTargetView(), "output.avi"));
                } else {
                    g_pFFmpegVideoRecorder->StopRecording();
                    SAFE_DELETE(g_pFFmpegVideoRecorder);
                }
            }
			// Space next step
			case VK_SPACE:
				g_bNextStep = !g_bNextStep;
        }
    }
}


//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse( bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
                       bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
                       int xPos, int yPos, void* pUserContext )
{
    // Track mouse movement if left mouse key is pressed
    {
        static int xPosSave = 0, yPosSave = 0;

        if (bLeftButtonDown)
        {
            // Accumulate deltas in g_viMouseDelta
            g_viMouseDelta.x += xPos - xPosSave;
            g_viMouseDelta.y += yPos - yPosSave;
        }    

        xPosSave = xPos;
        yPosSave = yPos;
    }
}


//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
                          bool* pbNoFurtherProcessing, void* pUserContext )
{
    // Send message to AntTweakbar first
    if (TwEventWin(hWnd, uMsg, wParam, lParam))
    {
        *pbNoFurtherProcessing = true;
        return 0;
    }

    // If message not processed yet, send to camera
    if(g_camera.HandleMessages(hWnd,uMsg,wParam,lParam))
    {
        *pbNoFurtherProcessing = true;
        return 0;
    }

    return 0;
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove( double dTime, float fElapsedTime, void* pUserContext )
{
    UpdateWindowTitle(L"Demo");

    // Move camera
    g_camera.FrameMove(fElapsedTime);

    g_fTimeAcc += fElapsedTime;

	if ( g_bSetup != g_bSetupOld )
	{
		g_iActivePoint = 0;
		return;
	}

	if ( g_iActivePoint > g_Points.size()-1 )
	{
		g_iActivePoint = 0;
	}

	if (g_iActivePoint != g_iOldActivePoint)
	{
		g_fActivePointMass = g_Points[g_iActivePoint].GetMass();
		g_iOldActivePoint = g_iActivePoint;
	}

	
	while ( g_fTimeAcc > g_fDeltaTime )
	{
		if ( g_bNextStep == true )
		{
			g_Points[g_iActivePoint].SetMass( g_fActivePointMass );
			
			// Euler or midpoint
			if ( g_bEuler == true )
			{
				EulerStep();
			}
			else
			{
				MidpointStep();
			}

			// Collision Detection
			// Followed http://studiofreya.com/3d-math-and-physics/simple-sphere-sphere-collision-detection-and-collision-response/
			for (int i = 0; i < g_Points.size()-1; i++)
			{
				XMVECTOR point1Position = XMLoadFloat3(&g_Points[i].GetPosition());
				
				for (int j = i + 1; j < g_Points.size(); j++)
				{
					XMVECTOR point2Position = XMLoadFloat3(&g_Points[j].GetPosition());

					XMVECTOR vDistance = point1Position - point2Position;
					float distance = XMVectorGetX(XMVector3Length(vDistance));
					float sumSphereSize = g_fSphereSize * (g_Points[i].GetMass() + g_Points[j].GetMass());
					
					// Poitns colliding
					if (distance < sumSphereSize)
					{
						//printf("Collision: %i - %i\n", i, j);

						// Collision Response

						// Take distance and normalize
						vDistance = XMVector3Normalize(vDistance);

						XMVECTOR velocity1 = XMLoadFloat3(&g_Points[i].GetVelocity());
						float x1 = XMVectorGetX(XMVector3Dot(vDistance, velocity1));

						XMVECTOR v1x = vDistance * x1;
						XMVECTOR v1y = velocity1 - v1x;

						XMVECTOR velocity2 = XMLoadFloat3(&g_Points[j].GetVelocity());
						float x2 = XMVectorGetX(XMVector3Dot(-vDistance, velocity2));

						XMVECTOR v2x = - vDistance * x2;
						XMVECTOR v2y = velocity2 - v2x;

						XMVECTOR result1 = 
							v1x * ((g_Points[i].GetMass() - g_Points[j].GetMass()) / (g_Points[i].GetMass() + g_Points[j].GetMass())) +
							v2x * ((2 * g_Points[j].GetMass()) / (g_Points[i].GetMass() + g_Points[j].GetMass())) +
							v1y;

						XMVECTOR result2 =
							v1x * ((2 * g_Points[i].GetMass() ) / (g_Points[i].GetMass() + g_Points[j].GetMass())) +
							v2x * ((g_Points[i].GetMass() - g_Points[j].GetMass()) / (g_Points[i].GetMass() + g_Points[j].GetMass())) +
							v2y;

						XMFLOAT3 point1Result;
						XMStoreFloat3(&point1Result,result1);
						g_Points[i].SetVelocity(point1Result);

						XMFLOAT3 point2Result;
						XMStoreFloat3(&point2Result, result2);
						g_Points[j].SetVelocity(point2Result);


					}
				}
			}

		}

		g_Time += g_fDeltaTime;
		g_fTimeAcc -= g_fDeltaTime;
	}

    // Update effects with new view + proj transformations
    g_pEffectPositionColor      ->SetView      (g_camera.GetViewMatrix());
    g_pEffectPositionColor      ->SetProjection(g_camera.GetProjMatrix());

    g_pEffectPositionNormal     ->SetView      (g_camera.GetViewMatrix());
    g_pEffectPositionNormal     ->SetProjection(g_camera.GetProjMatrix());

    g_pEffectPositionNormalColor->SetView      (g_camera.GetViewMatrix());
    g_pEffectPositionNormalColor->SetProjection(g_camera.GetProjMatrix());

    // Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
    if (g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0)
    {
        // Calcuate camera directions in world space
        XMMATRIX viewInv = XMMatrixInverse(nullptr, g_camera.GetViewMatrix());
        XMVECTOR camRightWorld = XMVector3TransformNormal(g_XMIdentityR0, viewInv);
        XMVECTOR camUpWorld    = XMVector3TransformNormal(g_XMIdentityR1, viewInv);

        // Add accumulated mouse deltas to movable object pos
        XMVECTOR vMovableObjectPos = XMLoadFloat3(&g_vfMovableObjectPos);

        float speedScale = 0.50f;
		vMovableObjectPos = XMVectorSet(0, 0, 0, 0);
        vMovableObjectPos = XMVectorAdd(vMovableObjectPos,  speedScale * (float)g_viMouseDelta.x * camRightWorld);
        vMovableObjectPos = XMVectorAdd(vMovableObjectPos, -speedScale * (float)g_viMouseDelta.y * camUpWorld);

        XMStoreFloat3(&g_vfMovableObjectPos, vMovableObjectPos);
        
        // Reset accumulated mouse deltas
        g_viMouseDelta = XMINT2(0,0);
    }
}

//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
                                  double fTime, float fElapsedTime, void* pUserContext )
{
    HRESULT hr;

    // Clear render target and depth stencil
    float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };




    ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
    ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
    pd3dImmediateContext->ClearRenderTargetView( pRTV, ClearColor );
    pd3dImmediateContext->ClearDepthStencilView( pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0 );


	if ( g_bSetup != g_bSetupOld )
	{
		g_bNextStep = false;
		g_bSetupOld = g_bSetup;
		g_iActivePoint = 0;
		InitMassSpringSystem( pd3dImmediateContext );
		
	}

    // Draw floor
    DrawFloor(pd3dImmediateContext);

    // Draw axis box
    //DrawBoundingBox(pd3dImmediateContext);
    
    // Draw spring
    DrawSpring(pd3dImmediateContext);

    // Draw speheres
    if (g_bDrawSpheres) DrawPoints(pd3dImmediateContext);


    // Draw GUI
    TwDraw();

    if (g_pFFmpegVideoRecorder) 
    {
        V(g_pFFmpegVideoRecorder->AddFrame(pd3dImmediateContext, DXUTGetD3D11RenderTargetView()));
    }
}

//--------------------------------------------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
#if defined(DEBUG) | defined(_DEBUG)
    // Enable run-time memory check for debug builds.
    // (on program exit, memory leaks are printed to Visual Studio's Output console)
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

#ifdef _DEBUG
    std::wcout << L"---- DEBUG BUILD ----\n\n";
#endif

    // Set general DXUT callbacks
    DXUTSetCallbackMsgProc( MsgProc );
    DXUTSetCallbackMouse( OnMouse, true );
    DXUTSetCallbackKeyboard( OnKeyboard );

    DXUTSetCallbackFrameMove( OnFrameMove );
    DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );

    // Set the D3D11 DXUT callbacks
    DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
    DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
    DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
    DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
    DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
    DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

    // Init camera
    XMFLOAT3 eye(0.0f, 1.0f, -4.0f);
    XMFLOAT3 lookAt(0.0f, 0.0f, 0.0f);
    g_camera.SetViewParams(XMLoadFloat3(&eye), XMLoadFloat3(&lookAt));
    g_camera.SetButtonMasks(MOUSE_MIDDLE_BUTTON, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON);


    // Init DXUT and create device
    DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
    //DXUTSetIsInGammaCorrectMode( false ); // true by default (SRGB backbuffer), disable to force a RGB backbuffer
    DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
    DXUTCreateWindow( L"Mass Spring System" );
    DXUTCreateDevice( D3D_FEATURE_LEVEL_10_0, true, 1280, 960 );
    
    DXUTMainLoop(); // Enter into the DXUT render loop

    DXUTShutdown(); // Shuts down DXUT (includes calls to OnD3D11ReleasingSwapChain() and OnD3D11DestroyDevice())

    return DXUTGetExitCode();
}
